# sisop-praktikum-modul-3-2023-WS-F06

## Soal Nomor 1
**Library**
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#define MAX_SIZE 256 // maksimum ukuran karakter
```
**Fungsi-Fungsi**
```
struct Node {
    char character;
    int frequency;
    struct Node *left;
    struct Node *right;
};

struct MinHeap {
    int size;
    int capacity;
    struct Node **array;
};

//Membuat dan menginisialisasi node dalam pohon Huffman
struct Node *createNode(char character, int frequency) {
    struct Node *node = (struct Node *)malloc(sizeof(struct Node));
    node->character = character;
    node->frequency = frequency;
    node->left = NULL;
    node->right = NULL;
    return node;
}

//Membuat dan menginisialisasi min heap yang akan digunakan dalam algoritma Huffman
struct MinHeap *createMinHeap(int capacity) {
    struct MinHeap *minHeap = (struct MinHeap *)malloc(sizeof(struct MinHeap));
    minHeap->size = 0;
    minHeap->capacity = capacity;
    minHeap->array = (struct Node **)malloc(capacity * sizeof(struct Node *));
    return minHeap;
}

//Menukar posisi dua node dalam min heap
void swapNodes(struct Node **a, struct Node **b) {
    struct Node *temp = *a;
    *a = *b;
    *b = temp;
}

//Melakukan operasi heapify pada min heap
void minHeapify(struct MinHeap *minHeap, int idx) {
    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;

    if (left < minHeap->size && minHeap->array[left]->frequency < minHeap->array[smallest]->frequency) {
        smallest = left;
    }

    if (right < minHeap->size && minHeap->array[right]->frequency < minHeap->array[smallest]->frequency) {
        smallest = right;
    }

    if (smallest != idx) {
        swapNodes(&minHeap->array[idx], &minHeap->array[smallest]);
        minHeapify(minHeap, smallest);
    }
}

//Mengembalikan 1 jika ukuran heap adalah 1, dan 0 jika tidak
int isSizeOne(struct MinHeap *minHeap) {
    return (minHeap->size == 1);
}

//Mengeluarkan (extract) node dengan frekuensi terkecil dari min heap.
struct Node *extractMin(struct MinHeap *minHeap) {
    struct Node *temp = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1];
    --minHeap->size;
    minHeapify(minHeap, 0);
    return temp;
}

//Menyisipkan (insert) sebuah node baru ke dalam min heap.
void insertMinHeap(struct MinHeap *minHeap, struct Node *node) {
    ++minHeap->size;
    int i = minHeap->size - 1;
    while (i && node->frequency < minHeap->array[(i - 1) / 2]->frequency) {
        minHeap->array[i] = minHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }
    minHeap->array[i] = node;
}

//Membangun min heap dari array node-node yang ada dalam heap.
void buildMinHeap(struct MinHeap *minHeap) {
    int n = minHeap->size - 1;
    int i;
    for (i = (n - 1) / 2; i >= 0; --i) {
        minHeapify(minHeap, i);
    }
}

//Membangun pohon Huffman berdasarkan frekuensi karakter yang diberikan
struct Node *buildHuffmanTree(int *freq) {
    struct Node *left, *right, *top;
    struct MinHeap *minHeap = createMinHeap(MAX_SIZE);

    for (int i = 0; i < MAX_SIZE; ++i) {
        if (freq[i] > 0) {
                        insertMinHeap(minHeap, createNode((char)i, freq[i]));
        }
    }

    buildMinHeap(minHeap);

    while (!isSizeOne(minHeap)) {
        left = extractMin(minHeap);
        right = extractMin(minHeap);

        top = createNode('$', left->frequency + right->frequency);
        top->left = left;
        top->right = right;

        insertMinHeap(minHeap, top);
    }

    struct Node *root = extractMin(minHeap);
    return root;
}

//Melakukan encoding Huffman pada pohon Huffman yang telah dibangun
void encodeHuffman(struct Node *root, char *code, int top, char **huffmanCodes) {
    if (root->left) {
        code[top] = '0';
        encodeHuffman(root->left, code, top + 1, huffmanCodes);
    }

    if (root->right) {
        code[top] = '1';
        encodeHuffman(root->right, code, top + 1, huffmanCodes);
    }

    if (!root->left && !root->right) {
        code[top] = '\0';
        huffmanCodes[root->character] = strdup(code);
    }
}

//Melakukan kompresi pada file 
void compressFile(FILE *file, struct Node *root, int *freq, int fd[]) {
    char *huffmanCodes[MAX_SIZE] = {0};
    char code[MAX_SIZE];

    encodeHuffman(root, code, 0, huffmanCodes);

    rewind(file);

    char bitBuffer = 0;
    int bitCount = 0;

    int c;
    while ((c = fgetc(file)) != EOF) {
        if (c >= 'A' && c <= 'Z') {
            char *huffmanCode = huffmanCodes[c];
            int i = 0;
            while (huffmanCode[i] != '\0') {
                bitBuffer <<= 1;
                if (huffmanCode[i] == '1') {
                    bitBuffer |= 1;
                }
                ++bitCount;

                if (bitCount == 8) {
                    write(fd[1], &bitBuffer, sizeof(char));
                    bitBuffer = 0;
                    bitCount = 0;
                }

                ++i;
            }
        }
    }

    if (bitCount > 0) {
        bitBuffer <<= (8 - bitCount);
        write(fd[1], &bitBuffer, sizeof(char));
    }

    
// Mengirim sinyal terminasi untuk menandakan akhir dari proses kompresi
    char terminationSignal = '\0';
    write(fd[1], &terminationSignal, sizeof(char));
}
```
**Main**

Jika argumen bukan 2, maka tidak sesuai. Menggunakan _printf_ untuk mencetak pesan dan mengambil data dari file.
```
if (argc != 2) {
        printf("Usage: %s [filename]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
```

Pada _fd[2]_ digunakan 2 elemen array sebagai file descriptor. _fd[0]_ untuk '_read end_ ' dan _fd[1]_ untuk '_write end_ '. _pid_ sebagai penyimpanan nilai PID dari child process. Dan _char buffer_ digunakan untuk menyimpan data yang dibaca / ditulis melalui pipe.
```
int fd[2];
    pid_t pid;
    char buffer[MAX_SIZE];
```

Fungsi pipe untuk menghasilkan nilai 0 jika berhasil atau -1 jika terjadi kesalahan. Jika gagal akan menampilkan pesan kesalahan menggunakan _perror("pipe")_.
```
if (pipe(fd) == -1) {
        perror("pipe");
        exit(EXIT_FAILURE);
    }
```

**a. Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.** 


Membuka file yang diberikan sebagai argumen baris perintah dengan metode baca 'r'.
```
FILE *file = fopen(argv[1], "r");
```

Hitung frekuensi kemunculan huruf dan pada code ini juga dilakukan pengecekan huruf kecil atau besar. Jika ada huruf kecil maka akan dijadikan huruf besar.
```
// hitung frekuensi kemunculan huruf
        int freq[MAX_SIZE] = {0};
        int c;
        while ((c = fgetc(file)) != EOF) {
            if (c >= 'a' && c <= 'z') {
                c -= 32; // ubah huruf kecil ke huruf besar
            }
            if (c >= 'A' && c <= 'Z') {
                freq[c]++;
            }
        }
```

Kirim hasil perhitungan frekuensi ke child process
```
// kirim frekuensi ke child process
        char freq_str[MAX_SIZE];
        for (int i = 0; i < MAX_SIZE; i++) {
            if (freq[i] > 0) {
                sprintf(freq_str, "%d:%d\n", i, freq[i]);
                write(fd[1], freq_str, strlen(freq_str));
            }
        }
```

**b. Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.**

```
if (pid == 0) { // child process
        close(fd[1]); // tutup write end
        FILE *out = fopen("compressed.txt", "w");

        // baca frekuensi dari parent process
        while (read(fd[0], buffer, MAX_SIZE) > 0) {
            // lakukan kompresi Huffman

```

**c. Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.**

Simpan Huffman tree ke file terkompresi (compressed.txt)
```
// kemudian tulis ke file compressed.txt
            fprintf(out, "%s", buffer);
```

Kirim code ke program dekompresi menggunakan pipe
```
// Buka file compressed.txt yang telah dihasilkan
        FILE *compressedFile = fopen("compressed.txt", "r");

        // Baca isi file compressed.txt dan kirim ke program dekompresi menggunakan pipe
        while (fgets(buffer, MAX_SIZE, compressedFile) != NULL) {
            write(fd[1], buffer, strlen(buffer));
        }
```
**Hasil** 

![Hasil 1abc](https://gitlab.com/sisopf06/sisop-praktikum-modul-3-2023-ws-f06/-/raw/main/Image/hasil1c.png)

##

## Soal Nomor 2
**a) Membuat program C dengan nama kalian. Membuat matrix 4x2 dan 2x5 dengan angka random kemudian dikalikan kedua matrixnya. Tampilkan matriks hasil perkalian tadi ke layar.**

```
int mat1[4][2], mat2[2][5], result[4][5];
    srand(time(NULL));
    // Mengisi matriks pertama dengan angka acak antara 1-5
    int i, j, k;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 2; j++) {
            mat1[i][j] = (rand() % 5) + 1;
        }
    }

    // Mengisi matriks kedua dengan angka acak antara 1-4
    for (i = 0; i < 2; i++) {
        for (j = 0; j < 5; j++) {
            mat2[i][j] = (rand() % 4) + 1;
        }
    }

    // Perkalian matriks
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            result[i][j] = 0;
            for (k = 0; k < 2; k++) {
                result[i][j] += mat1[i][k] * mat2[k][j];
            }
        }
    }

    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            printf("%d\t", result[i][j]);
        }
        printf("\n");
    }
```
- Membuat random number menggunakan srand(time(NULL)); agar random numbernya selalu acak.
- Pertama membuat matriks dengan ukuran 4x2. Kemudian memasukkan nilai secara acak menggunakan (rand() % 5) + 1; Karena di soal "rentang pada matriks pertama adalah 1-5" sehingga menggunakan modulus untuk operasinya, dan ditambah 1 karena modulus dimulai dari 0.
- Menggunakan implementasi yang sama untuk matrix ke dua dengan size 2x5.
- Kemudian mengkalikan matrix 1 dan 2 dengan rumus perkalian matrix "mat1[i][k] * mat2[k][j]". Jika sudah dikali, Hasilnya akan disimpan di result.
- Terakhir adalah menampilkan hasil perkaliannya dengan printf. di loop sebanyak size hasil matrix yaitu 4x5.

**b) Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. 
(Catatan: wajib menerapkan konsep shared memory)**

```
typedef struct {
    int matrix[4][5];
} hasil;
```
Mendeklarasikan struct hasil untuk menyimpan data matrix yang didapatkan dari program kalian.c

```
    key_t key = ftok("kalian.c", 'R');
    int shmid = shmget(key, sizeof(hasil), 0666);
    Hasil = (hasil *)shmat(shmid, (void *)0, 0);

    printf("Hasil perkalian matriks dari program kalian.c:\n");
    display_matrix(Hasil->matrix);

    // Melepaskan shared memory
    shmdt(Hasil);
```
- Mengakses shared memory yang sama dengan kalian.c
- Jika sudah diakses, akan diambil hasil dari perkalian di kalian.c dan disimpan didalam struct Hasil->matrix.
- Setelah didapat, hasil perkalian ditampilkan menggunakan fungsi display_matrix(Hasil->matrix), Dimana fungsi tersebut sama seperti fungsi untuk menampilkan hasil perkalian matrix di kalian.c
- Terakhir melepaskan shared memory.


**c) Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks. (Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)**

Pertama, buat fungsi perhitungan faktorialnya terlebih dahulu.
```
typedef struct {
    int row;
    int col;
} rowcol;

unsigned long long factorial(int n) {
    unsigned long long fact = 1;
    int i;
    for (i = 2; i <= n; i++) {
        fact *= i;
    }
    return fact;
}

void *calculate_factorial(void *arg) {
    rowcol *RowCol = (rowcol *)arg;
    int row = RowCol->row;
    int col = RowCol->col;

    unsigned long long fact = factorial(Hasil->matrix[row][col]);
    Hasil->matrix[row][col] = fact;

    free(RowCol);

    pthread_exit(NULL);
}
```
- Struct rowcol untuk menyimpan row dan column dari matrix yang nantinya akan dihitung di fungsi calculate_factorial().
- Untuk fungsi calculate_factorial(), pertama akan diambil row dan column dari yang sudah diberikan di fungsi main(). kemudian matrix akan dihitung di fungsi factorial(). Terakhir exit thread dengan pthread_exit().
- Untuk fungsi factorial(int n), Fungsi akan mengambil nilai matrix dan memfaktorialkannya dengan cara mengkalikan dengan nilai 2 hingga n.

```

    // Menunggu semua thread selesai
    for (i = 0; i < 4 * 5; i++) {
        pthread_join(threads[i], NULL);
    }

    printf("Hasil Faktorial:\n");
    display_matrix(Hasil->matrix);

```
- Menunggu semua thread selesai
- Jika sudah selesai semua, hasil akan ditampilkan dengan fungsi display_matix(Hasil->matrix);

**d) Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak.**

```
    clock_t start, end;
    double cpu_time_used;

    start = clock();
    // Mengakses shared memory
    key_t key = ftok("kalian.c", 'R');
    int shmid = shmget(key, sizeof(hasil), 0666);
    hasil *Hasil = (hasil *)shmat(shmid, (void *)0, 0);
```
- Pertama memulai waktu dengan clock() untuk memulai waktu perbandingan.
- Kemudian akses shared memory dengan kalian.c

```
    // Melakukan faktorisasi matriks
    int i, j;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            unsigned long long fact = factorial(Hasil->matrix[i][j]);
            Hasil->matrix[i][j] = fact;
        }
    }
```
Kemudian dilakukan faktorisasi setiap nilai matrix.
- Mengambil nilai matrix dengan cara loop [i][j]. kemudian dihitung faktorialnya dengan menggunakan fungsi factorial(). Jika sudah, hasil akan dibalikkan ke matrix aslinya.

```
    printf("\nHasil faktorisasi matriks:\n");
    display_matrix(Hasil->matrix);

    end = clock();
    cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;

    printf("Waktu eksekusi: %f detik\n", cpu_time_used);
    // Melepaskan shared memory
    shmdt(Hasil);
```
Terakhir adalah print hasil faktorisasi dan berapa lama waktu eksekusinya agar bisa dipakai untuk perbandingan.
- print hasil faktorisasi dengan fungsi display_matrix()
- end clock() untuk menghentikan waktu dan waktu eksekusinya di tampilkan.
- Setelah semuanya selesai, shared memory dilepaskan.

Untuk membandingkan, implementasi clock() yang sama juga dipakai di program cinta.c sehingga jika dirun akan diketahui perbandingannya.

**Perbandingan**

Waktu cinta.c :
![cinta.c](https://gitlab.com/sisopf06/sisop-praktikum-modul-3-2023-ws-f06/-/raw/main/Image/cintac.png)

Waktu sisop.c :
![sisop.c](https://gitlab.com/sisopf06/sisop-praktikum-modul-3-2023-ws-f06/-/raw/main/Image/sisopc.png)

Waktu program sisop.c lebih cepat dari waktu eksekusi cinta.c

## Soal Nomor 3

**a) membuat sistem stream (receiver) stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib).**

Solusi dari poin a adalah dengan membuat message queue di kedua program.
Dengan rincian adalah message queue sender dibuat di user.c dan recievernya dibuat di stream.c

>user.c
```
int running=1;
         int msgid;
         struct my_msg some_data;
         char buffer[50]; //array to store user input
         msgid=msgget((key_t)14534,0666|IPC_CREAT);
         if (msgid == -1) // -1 means the message queue is not created
         {
                 printf("Error in creating queue\n");
                 exit(0);
         }
```
- Membuat message queue dengan msgid

```
while(running)
         {
                 printf("Enter some text:\n");
                 fgets(buffer,50,stdin);
                 some_data.msg_type=1;
                 strcpy(some_data.some_text,buffer);
                 if(msgsnd(msgid,(void *)&some_data, MAX_TEXT,0)==-1) // msgsnd returns -1 if the message is not sent
                 {
                         printf("Msg not sent\n");
                 }
                 if(strncmp(buffer,"end",3)==0)
                 {
                         running=0;
                 }
         }
```
- Jika berhasil dibuat, program akan running secara loop/terus menerus.
- User akan menginputkan text command yang nanti akan dikirim ke reciever yaitu stream.c
- inputan user dikirim ke reciever dengan menggunakan msgsnd(msgid,(void *)&some_data, MAX_TEXT,0). Jika == -1 maka akan ada peringatan.
- Jika user ingin keluar dari program, maka harus menginputkan "end".

Untuk reciever di stream.c, sama seperti pembuatan message queue di user.c. Perbedaannya hanya di line 184:

>stream.c

```
msgrcv(msgid, (void *)&some_data, BUFSIZ, msg_to_rec, 0);
```
Kalau di user.c menggunakan msgsnd untuk mengirim, di stream.c menggunakan msgrcv untuk menerima pesan dari user.c

**b) mengirimkan perintah DECRYPT kemudian sistem stream akan melakukan decrypt/decode/konversi pada file song-playlist.json dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet.**

Untuk solusi pertama adalah menerima perintah DECRYPT yang nantinya akan dijalankan.
>stream.c
```
else if (strncmp(some_data.some_text, "DECRYPT", strlen("DECRYPT")) == 0)
    {
       Decrypt();
    }
```
- Menggunakan string compare untuk mengetahui apakah user menginputkan DECRYPT atau tidak. Jika iya akan dijalankan fungsi Decrypt().

Selanjutnya adalah mengkonversi file song-playlist.json. Untuk itu, yang pertama dilakukan adalah membuka file json dan playlist.txt
```
fp_in = fopen("song-playlist.json", "r");
fp_out = fopen("playlist.txt", "w");
```
- Membuka file song-playlist.json dan diread menggunakan "r". Karena file ini hanya akan di read saja.
- Membuka file playlist.txt dan diwrite menggunakan "w". Karena nantinya output dari file json akan ditulis di txt.

Selanjutnya adalah mengambil metode apa yang digunakan untuk dekripsinya dan apa judul lagu yang ingin didekripsi
```
    while (fgets(line, MAX_LINE_LENGTH, fp_in) != NULL) {
        if (strstr(line, "\"method\"") != NULL) {
            sscanf(line, "  \"method\": \"%[^\"]\",", method);
        }
        if (strstr(line, "\"song\"") != NULL) {
            sscanf(line, "  \"song\": \"%[^\"]\",", song);
```
- Akan dicek per line menggunakan fgets.
- Mencari apakah di line tersebut ada kata yang mengandung method atau tidak.
- Jika ada, maka akan di ambil metodenya menggunakan sscanf.
- Menggunakan cara yang sama untuk mengambil judul lagunya.

Jika sudah mendapatkan judul lagu dan metode yang dipakai untuk decryptnya. Selanjutnya adalah proses decrypt.
```
            if (strcmp(method, "base64") == 0) {
                sprintf(command, "echo '%s' | base64 --decode", song);
            } else if (strcmp(method, "rot13") == 0) {
                sprintf(command, "echo -n '%s' | tr '[A-Za-z]' '[N-ZA-Mn-za-m]'", song);
            } else if (strcmp(method, "hex") == 0) {
                sprintf(command, "echo '%s' | xxd -r -p", song);
            }
```
- Ketiga metode dekripsi ini menggunakan linux command, sehingga disini digunakan sprintf untuk menggabungkan judul lagu dengan command decryptnya.

Setelah judul lagu dah digabungkan dengan command dekripsinya, maka tinggal menggunakan linux command untuk dekripsinya
```
FILE *fp_pipe = popen(command, "r");
char decoded_song[MAX_SONG_LENGTH];
	    fgets(decoded_song, MAX_SONG_LENGTH, fp_pipe);
            pclose(fp_pipe);
            fprintf(fp_out, "%s\n", decoded_song);

```
- Menggunakan popen untuk bisa mengakses command di linux. - Kemudian membaca hasil decryptnya.
- Terakhir output dari decryptnya diprint menggunakan fprintf ke fp_out atau ke file playlist.txt

Terakhir, mengurutkan judul lagu sesuai alphabet menggunakan command sort.
> system("sort -o playlist.txt playlist.txt");

**c) user dapat mengirimkan perintah LIST, kemudian sistem stream akan menampilkan daftar lagu yang telah di-decrypt**

Untuk poin c, hanya menampilkan daftar lagu. Solusinya adalah dengan menggunakan system() dan menggunakan linux command cat untuk menampilkan isi file.
```
 system("cat playlist.txt");
```

**d)User juga dapat mengirimkan perintah PLAY \<SONG\>**

Solusi pertama adalah menerima perintah PLAY "judul" dari user dan memasukkan songnya ke Play(judul).

Karena yang dibutuhkan oleh fungsi Play() hanya judul saja, sehingga char yang tidak penting harus dihapus, yaitu kata play dan spasinya ("PLAY ") dan tanda petik di judulnya.

Solusinya :
```
char* song_title = some_data.some_text + strlen("PLAY "); 
       char petik = '\"';
       song_title[strcspn(song_title, "\n")] = 0;
       removePetik(song_title, petik);
       Play(song_title);
```
- Menghapus kata "PLAY " beserta spasinya dengan menambahkan strlen sehingga string akan dimulai dari ""judul"".
- Kemudian menghapus newline.
- Karena judul lagunya masih ada tanda petik, maka untuk mengilangkannya digunakan fungsi removePetik. Inti dari fungsi removePetik adalah mencari letak dimana tanda petik berada dan menghapusnya dengan menjadikan petik = '\0' atau null.
- Jika sudah hanya tersisa judul lagu, maka dapat dimasukkan ke fungsi Play().

Di fungsi Play() pertama akan dibuka file playlist.txt secara read untuk bisa mencari lagunya.
```
    char *filename = "playlist.txt";
    FILE *file = fopen(filename, "r");
```

Kemudian, mencari ada berapa lagu yang mengandung kata yang diinputkan oleh user.
```
while (fgets(line, 1000, file)) {
        char *song_title = strstr(line, judul);

        if (song_title != NULL) {
            num_songs++;
        }
    }
```
- Akan dicek per line apakah ada judul lagu yang mengandung kata yang diinputkan user. jika ada, maka num_song akan bertambah 1.

Jika sudah tau jumlah judul lagu yang dicari, maka tinggal membuat tampilannya.
```
    if (num_songs == 0) {
        printf("THERE IS NO SONG CONTAINING \"%s\".\n", judul);
    }

```
- Jika tidak ada judul yang mengandung kata yang diinputkan user, maka akan menampilkan "THERE IS NO SONG CONTAINING".

```
else if(num_songs == 1) {
	rewind(file);
	while (fgets(line, 1000, file)) {
        char *song_title = strstr(line, judul);

        if (song_title != NULL) {
          song_title[strcspn(song_title, "\n")] = 0;
	  printf("USER %d PLAYING \"%s\"\n", getuid(), song_title);
        }
    }
```
- Jika hanya satu, playlist.txt akan dibaca ulang untuk mengambil judul lagunya. Jika judul lagunya sudah ketemu, akan print "User \<User ID\> PLAYING judul_lagu"

```
printf("THERE ARE %d SONG CONTAINING \"%s\" :\n", num_songs, judul);
	rewind(file);
        num_songs = 0;
        while (fgets(line, 1000, file)) {
        char *song_title = strstr(line, judul);

        if (song_title != NULL) {
	   num_songs++;
	   printf("%d. %s", num_songs, line);
        }
    }
```
- Jika lebih dari satu, playlist.txt akan dibaca ulang menggunakan rewind(file) dan akan diambil semua lagunya. Kemudian akan diprint nomor dan judul lagunya.

**e) User mengirimkan perintah ADD \<SONG\>. pabila terdapat lagu yang sama maka sistem akan meng-output-kan “SONG ALREADY ON PLAYLIST”**

Pertama open playlist.txt seperti biasa untuk membaca apakah judul lagu yang ingin ditambahkan sudah ada di file atau belum.
```
FILE* file = fopen("playlist.txt", "r");

int song_already_on_playlist = 0;
    while (fgets(buffer, sizeof(buffer), file)) {
        if (strstr(buffer, judul) != NULL) {
            song_already_on_playlist = 1;
            break;
        }
    }
```
- Membuka file playlist.txt
- Untuk mencari apakah judul lagu yang ingin dimasukkan sudah ada atau belum, saya menggunakan strstr untuk mencaari substring. Jika judul lagunya sudah ada, variabel song_already_on_playlist akan dijadikan 1.

Setelah cek apakah judul lagu sudah ada atau belum, program akan menentukan apakah menambahkan lagu atau tidak.
```
 if (song_already_on_playlist) {
        printf("SONG ALREADY ON PLAYLIST\n");
    }
```
- Jika sudah ada, maka akan diprint "SONG ALREADY ON PLAYLIST"

```
else {
        file = fopen("playlist.txt", "a");
        if (file == NULL) {
            printf("Error: could not open playlist file\n");
            return 0;
        }
        fprintf(file, "%s\n", judul);
        printf("USER %d ADD %s\n", getuid(), judul);

        // Close the file
        fclose(file);
    }
```
- Jika judul belum ada, playlist.txt akan dibuka kembali dengan mode "a" atau append. Kemudian menambahkan judul lagu dengan menggunakan fprintf. Kemudian program akan menampilkan USER ADD judul. dan file playlist diclose.

**g)Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".**

Untuk Solusinya
```
else
    {
       printf("UNKNOWN COMMAND");
    }
```
- Jika command tidak ada, maka akan print UNKNOWN COMMAND

>**Revisi Soal Nomor 3**

Pada fungsi Play(), jika user menginputkan "Break" akan ada 3 judul yang muncul yaitu, 
1. Break Free - Ariana Grande
2. Break Your Heart - Taio Cruz
3. Breaks Like a Heart (feat. Miley Cyrus) - Mark Ronson

Masalahnya ada pada judul yang ketiga. "Break" dan "Breaks" adalah 2 kata yang berbeda sehingga seharusnya yang muncul hanya 2 judul lagu yaitu "Break Free - Ariana Grande" dan "Break Your Heart - Taio Cruz".

Untuk revisinya ada di bagian menghitung jumlah judul lagu yang sesuai di fungsi play().
```
if ((song_title == strstr(song_title, judul) && 
                (song_title + strlen(judul) == NULL || !isalpha(song_title[strlen(judul)]))) &&
                (song_title == line || !isalpha(song_title[-1]))) {
                num_songs++;
            }
```
- Jika substring ditemukan dan substringnya benar benar sesuai, maka num_songs akan bertambah.
- Kode ini juga diimplementasikan untuk revisi yang jika judul yang sesuai ada lebih dari 1 (if num_songs > 1).

Setelah diubah, hasilnya akan menjadi
1. Break Free - Ariana Grande
2. Break Your Heart - Taio Cruz

## Soal Nomor 4
1. Poin a

**Library**
```
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

```
**Global Variabel**
```
int status;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int donlod = 0;

```
- int status merupakan sebuah flag untuk child proses 
- cond merupakan sebuah inisialisasi untuk kondisi dari sinkronisasi menggunakan mutex, dengan download sebagai flag atau penghubung dalam sinkronisasi
- mutex digunakan untuk proses sinkronisasi antar thread, untuk melindungi resource dari thread yaitu dengan lock dan unlock

**Fungsi Download**
```
void *download(void *arg){
	char link[] ="https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
	pid_t pid = fork();
	if(pid == 0){
		execlp("wget", "wget","--no-check-certificate", "-O", "hehe.zip", link, NULL);
		exit(1);
	}
	wait(&status);

	pthread_mutex_lock(&mutex);
	donlod = 1;
	pthread_cond_signal(&cond);
	pthread_mutex_unlock(&mutex);

	pthread_exit(NULL);
}
```
- --no-check-certificate digunakan untuk mengabaikan certificate ketika membuat request pada HTTPS sehingga apabila certificate tersebut sudah expire, maka masih dapat berjalan
- mutex_lock digunakan untuk mengunci mutex agar dapat set flag download(donlod) menjadi tersebut
- Sinyal akan dikirimkan kepada cond agar thread lain baru dapat berjalan setelah flag bernilai true

**Fungsi Unzip**
```
void *unzip(void *arg){
	pthread_mutex_lock(&mutex);
	while(!donlod){
		pthread_cond_wait(&cond, &mutex);
	}
	pthread_mutex_unlock(&mutex);

	pid_t pid = fork();
	if(pid == 0){
		execlp("unzip", "unzip", "-q", "hehe.zip", NULL);
		exit(1);
	}
	wait(&status);

	pthread_exit(NULL);
}
```
- Fungsi akan mengunci mutex dan menunggu hingga download bernilai true
- cond_wait akan menunggu sinyal melalui variabel cond dan mutex yang terdapat pada fungsi download hingga sinyal download bernilai true dikeluarkan
- setelah mutex dilepaskan, maka akan menjelaskan proses unzip file

**Fungsi Main**
```
int main(){
	pthread_t threads[2];

	int downres = pthread_create(&threads[0], NULL, download, NULL);
	if(downres){
		printf("download error");
		exit(1);
	}

	int unzipres = pthread_create(&threads[1], NULL, unzip, NULL);
	if(unzipres){
		printf("unzip error");
		exit(1);
	}

	int i;
	for(i=0; i<2; i++){
		pthread_join(threads[i], NULL);
	}

	return 0;
}

```
- akan membuat thread dengan array sebesar 2
- downres akan membuat thread dan menjalankan fungsi download pada array 0
- jika download masih bernilai true, maka merupakan indikasi terdapat kegagalan dalam proses fungsi download atau pada pembuatan thread
- unzipres akan membuat thread dan menjalankan fungsi unzip pada array berikutnya
- jika unzipres masih bernilai true, maka terdapat kegagalan diantara fungsi unzip atau pembuatan thread
- Melakukan proses join thread dengan menggunakan for loop pada array threads, hal ini agar thread dapat menyelesaikan eksekusinya terlebih dahulu sebelum menuju main thread
