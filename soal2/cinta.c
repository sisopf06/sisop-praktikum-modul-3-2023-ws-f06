#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <time.h>

typedef struct {
    int matrix[4][5];
} hasil;

typedef struct {
    int row;
    int col;
} rowcol;

hasil *Hasil;

void display_matrix(int mat[4][5]) {
    int i, j;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            printf("%d\t", mat[i][j]);
        }
        printf("\n");
    }
}

unsigned long long factorial(int n) {
    unsigned long long fact = 1;
    int i;
    for (i = 2; i <= n; i++) {
        fact *= i;
    }
    return fact;
}

void *calculate_factorial(void *arg) {
    rowcol *RowCol = (rowcol *)arg;
    int row = RowCol->row;
    int col = RowCol->col;

    unsigned long long fact = factorial(Hasil->matrix[row][col]);
    Hasil->matrix[row][col] = fact;

    free(RowCol);

    pthread_exit(NULL);
}

int main() {
    // Mengakses shared memory
clock_t start, end;
    double cpu_time_used;

    start = clock();

    key_t key = ftok("kalian.c", 'R');
    int shmid = shmget(key, sizeof(hasil), 0666);
    Hasil = (hasil *)shmat(shmid, (void *)0, 0);

    // Menampilkan hasil perkalian matriks dari shared memory
    printf("Hasil perkalian matriks dari program kalian.c:\n");
    display_matrix(Hasil->matrix);

    pthread_t threads[4 * 5];
    int threadIndex = 0;
    int i, j;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            rowcol *RowCol = (rowcol *)malloc(sizeof(rowcol));
            RowCol->row = i;
            RowCol->col = j;
            pthread_create(&threads[threadIndex++], NULL, calculate_factorial, (void *)RowCol);
        }
    }

    // Menunggu semua thread selesai
    for (i = 0; i < 4 * 5; i++) {
        pthread_join(threads[i], NULL);
    }

    printf("Hasil Faktorial:\n");
    display_matrix(Hasil->matrix);

    end = clock();
    cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;

    printf("Waktu eksekusi: %f detik\n", cpu_time_used);

    // Melepaskan shared memory
    shmdt(Hasil);

    return 0;
}
