#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

typedef struct{
    int matrix[4][5];
}hasil;

int main() {
    int mat1[4][2], mat2[2][5], result[4][5];

    srand(time(NULL));
    // Mengisi matriks pertama dengan angka acak antara 1-5
    int i, j, k;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 2; j++) {
            mat1[i][j] = (rand() % 5) + 1;
        }
    }

    // Mengisi matriks kedua dengan angka acak antara 1-4
    for (i = 0; i < 2; i++) {
        for (j = 0; j < 5; j++) {
            mat2[i][j] = (rand() % 4) + 1;
        }
    }

    // Perkalian matriks
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            result[i][j] = 0;
            for (k = 0; k < 2; k++) {
                result[i][j] += mat1[i][k] * mat2[k][j];
            }
        }
    }

    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            printf("%d\t", result[i][j]);
        }
        printf("\n");
    }


    // Membuat shared memory
    key_t key = ftok("kalian.c", 'R');
    int shmid = shmget(key, sizeof(hasil), 0666 | IPC_CREAT);
    hasil *Hasil = (hasil *)shmat(shmid, (void *)0, 0);

    // Menyalin hasil perkalian matriks ke shared memory
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            Hasil->matrix[i][j] = result[i][j];
        }
    }

    // Melepaskan shared memory
    shmdt(Hasil);

    return 0;
}
