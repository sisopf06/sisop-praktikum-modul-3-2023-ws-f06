#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

typedef struct {
    int matrix[4][5];
} hasil;


void display_matrix(int mat[4][5]) {
    int i, j;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            printf("%d\t", mat[i][j]);
        }
        printf("\n");
    }
}

unsigned long long factorial(int n) {
    unsigned long long fact = 1;
    int i;
    for (i = 2; i <= n; i++) {
        fact *= i;
    }
    return fact;
}

int main() {
    clock_t start, end;
    double cpu_time_used;

    start = clock();
    // Mengakses shared memory
    key_t key = ftok("kalian.c", 'R');
    int shmid = shmget(key, sizeof(hasil), 0666);
    hasil *Hasil = (hasil *)shmat(shmid, (void *)0, 0);

    // Menampilkan hasil perkalian matriks dari shared memory sebelum faktorisasi
    printf("Hasil perkalian matriks dari program kalian.c:\n");
    display_matrix(Hasil->matrix);

    // Melakukan faktorisasi matriks
    int i, j;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            unsigned long long fact = factorial(Hasil->matrix[i][j]);
            Hasil->matrix[i][j] = fact;
        }
    }

    printf("\nHasil faktorisasi matriks:\n");
    display_matrix(Hasil->matrix);

    end = clock();
    cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;

    printf("Waktu eksekusi: %f detik\n", cpu_time_used);
    // Melepaskan shared memory
    shmdt(Hasil);

    return 0;
}
