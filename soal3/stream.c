#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/msg.h>
#include<ctype.h>

#define MAX_SONG_LENGTH 256
#define MAX_METHOD_LENGTH 16
#define MAX_LINE_LENGTH 1024

struct my_msg{
         long int msg_type;
         char some_text[BUFSIZ];
};

int Decrypt() {
FILE *fp_in, *fp_out;
    char line[MAX_LINE_LENGTH];
    char method[MAX_METHOD_LENGTH], song[MAX_SONG_LENGTH];
    char command[MAX_LINE_LENGTH];

    fp_in = fopen("song-playlist.json", "r");
    if (fp_in == NULL) {
        printf("Error: Unable to open input file.\n");
        exit(1);
    }

    fp_out = fopen("playlist.txt", "w");
    if (fp_out == NULL) {
        printf("Error: Unable to open output file.\n");
        exit(1);
    }

    while (fgets(line, MAX_LINE_LENGTH, fp_in) != NULL) {
        if (strstr(line, "\"method\"") != NULL) {
            sscanf(line, "  \"method\": \"%[^\"]\",", method);
        }
        if (strstr(line, "\"song\"") != NULL) {
            sscanf(line, "  \"song\": \"%[^\"]\",", song);
            if (strcmp(method, "base64") == 0) {
                sprintf(command, "echo '%s' | base64 --decode", song);
            } else if (strcmp(method, "rot13") == 0) {
                sprintf(command, "echo -n '%s' | tr '[A-Za-z]' '[N-ZA-Mn-za-m]'", song);
            } else if (strcmp(method, "hex") == 0) {
                sprintf(command, "echo '%s' | xxd -r -p", song);
            } else {
                printf("Error: Unknown method: %s\n", method);
                exit(1);
            }
            FILE *fp_pipe = popen(command, "r");
            if (fp_pipe == NULL) {
                printf("Error: Unable to execute command.\n");
                exit(1);
            }
            char decoded_song[MAX_SONG_LENGTH];
	    fgets(decoded_song, MAX_SONG_LENGTH, fp_pipe);
            pclose(fp_pipe);
            fprintf(fp_out, "%s\n", decoded_song);
        }
    }

    fclose(fp_in);
    fclose(fp_out);

    system("sort -o playlist.txt playlist.txt");

    return 0;
}

int Play(char *judul) {
    char *filename = "playlist.txt";
    FILE *file = fopen(filename, "r");
    char line[1000];
    int num_songs = 0;

    while (fgets(line, 1000, file)) {
        char *song_title = strstr(line, judul);

        if (song_title != NULL) {
        if ((song_title == strstr(song_title, judul) && 
                (song_title + strlen(judul) == NULL || !isalpha(song_title[strlen(judul)]))) &&
                (song_title == line || !isalpha(song_title[-1]))) {
                num_songs++;
            }
        }
    }

    if (num_songs == 0) {
        printf("THERE IS NO SONG CONTAINING \"%s\".\n", judul);
    }
    else if(num_songs == 1) {
	rewind(file);
	while (fgets(line, 1000, file)) {
        char *song_title = strstr(line, judul);

        if (song_title != NULL) {
          song_title[strcspn(song_title, "\n")] = 0;
	  printf("USER %d PLAYING \"%s\"\n", getuid(), song_title);
        }
    }
    }
    else {
        printf("THERE ARE %d SONG CONTAINING \"%s\" :\n", num_songs, judul);
	rewind(file);
        num_songs = 0;
        while (fgets(line, 1000, file)) {
        char *song_title = strstr(line, judul);

       if (song_title != NULL) {
                if ((song_title == strstr(song_title, judul) && 
                (song_title + strlen(judul) == NULL || !isalpha(song_title[strlen(judul)]))) &&
                (song_title == line || !isalpha(song_title[-1]))){
                num_songs++;
                printf("%d. %s", num_songs, line);
            }
        }
    }
   }
    fclose(file);
    return 0;
}

int List(){
 system("cat playlist.txt");
}

int Add(char *judul) {
    FILE* file = fopen("playlist.txt", "r");

    // Check if the file was opened successfully
    if (file == NULL) {
        printf("Error: could not open playlist file\n");
        return 0;
    }

    // Search for the song title in the playlist
    char buffer[256];
    int song_already_on_playlist = 0;
    while (fgets(buffer, sizeof(buffer), file)) {
        if (strstr(buffer, judul) != NULL) {
            song_already_on_playlist = 1;
            break;
        }
    }

    // Close the file
    fclose(file);

    // Add the song title to the playlist and display the appropriate message
    if (song_already_on_playlist) {
        printf("SONG ALREADY ON PLAYLIST\n");
    } else {
        // Open the text file in append mode
        file = fopen("playlist.txt", "a");
        if (file == NULL) {
            printf("Error: could not open playlist file\n");
            return 0;
        }
        fprintf(file, "%s\n", judul);
        printf("USER %d ADD %s\n", getuid(), judul);

        // Close the file
        fclose(file);
    }
  }

void removePetik(char *str, char c) {
    int i, j;
    int len = strlen(str);
    for (i = j = 0; i < len; i++) {
        if (str[i] != c) {
            str[j++] = str[i];
        }
    }
    str[j] = '\0';
}


int main() {

int running=1;
int msgid;
struct my_msg some_data;
long int msg_to_rec=0;
  msgid=msgget((key_t)14534,0666|IPC_CREAT);
  while (running)
  {
    msgrcv(msgid, (void *)&some_data, BUFSIZ, msg_to_rec, 0);
    if (strncmp(some_data.some_text, "end", 3) == 0)
    {
      running = 0;
    }
    else if (strncmp(some_data.some_text, "DECRYPT", strlen("DECRYPT")) == 0)
    {
       Decrypt();
    }
    else if (strncmp(some_data.some_text, "LIST", strlen("LIST")) == 0)
    {
       List();
    }
    else if (strncmp(some_data.some_text, "PLAY ", strlen("PLAY ")) == 0)
    {
       char* song_title = some_data.some_text + strlen("PLAY "); // mengambil ju>
       char petik = '\"';
       song_title[strcspn(song_title, "\n")] = 0;
       removePetik(song_title, petik);
       Play(song_title);
    }

    else if (strncmp(some_data.some_text, "ADD ", strlen("ADD ")) == 0)
    {
       char* song_title = some_data.some_text + strlen("ADD "); // mengambil judul lagu dari pesan
       char petik = '\"';
       song_title[strcspn(song_title, "\n")] = 0;
       removePetik(song_title, petik);
       Add(song_title);
    }
    else
    {
       printf("UNKNOWN COMMAND");
    }

}

}
