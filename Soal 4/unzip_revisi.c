#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

int status;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int donlod = 0;

void *download(void *arg){
	char link[] ="https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
	pid_t pid = fork();
	if(pid == 0){
		execlp("wget", "wget","--no-check-certificate", "-O", "hehe.zip", link, NULL);
		exit(1);
	}
	wait(&status);

	pthread_mutex_lock(&mutex);
	donlod = 1;
	pthread_cond_signal(&cond);
	pthread_mutex_unlock(&mutex);

	pthread_exit(NULL);
}

void *unzip(void *arg){
	pthread_mutex_lock(&mutex);
	while(!donlod){
		pthread_cond_wait(&cond, &mutex);
	}
	pthread_mutex_unlock(&mutex);

	pid_t pid = fork();
	if(pid == 0){
		execlp("unzip", "unzip", "-q", "hehe.zip", NULL);
		exit(1);
	}
	wait(&status);

	pthread_exit(NULL);
}

int main(){
	pthread_t threads[2];

	int downres = pthread_create(&threads[0], NULL, download, NULL);
	if(downres){
		printf("download error");
		exit(1);
	}

	int unzipres = pthread_create(&threads[1], NULL, unzip, NULL);
	if(unzipres){
		printf("unzip error");
		exit(1);
	}

	int i;
	for(i=0; i<2; i++){
		pthread_join(threads[i], NULL);
	}

	return 0;
}
