#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

void *download(void *arg){
	int downres = system("wget -O hehe.zip https://drive.google.com/u/0/ic?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download");
	if (downres){
		printf("error downloading");
		exit(1);
	}
	pthread_exit(NULL);
}

void *unzip(void *arg){
	int unres = system("unzip -q hehe.zip");
	if (unres){
		printf("error unzip");
		exit(1);
	}
	pthread_exit(NULL);
}

int main(){
	pthread_t threads[2];

	int downres = pthread_create(&threads[0], NULL, download, NULL);
	if(downres){
		printf("download error");
		exit(1);
	}

	int unzipres = pthread_create(&threads[1], NULL, unzip, NULL);
	if(unzipres){
		printf("unzip error");
		exit(1);
	}

	int i;
	for(i=0; i<2; i++){
		pthread_join(threads[i], NULL);
	}

}
