#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>

#define MAX_SIZE 256 // maksimum ukuran karakter

struct Node {
    char character;
    int frequency;
    struct Node *left;
    struct Node *right;
};

struct MinHeap {
    int size;
    int capacity;
    struct Node **array;
};

//Membuat dan menginisialisasi node dalam pohon Huffman
struct Node *createNode(char character, int frequency) {
    struct Node *node = (struct Node *)malloc(sizeof(struct Node));
    node->character = character;
    node->frequency = frequency;
    node->left = NULL;
    node->right = NULL;
    return node;
}

//Membuat dan menginisialisasi min heap yang akan digunakan dalam algoritma Huffman
struct MinHeap *createMinHeap(int capacity) {
    struct MinHeap *minHeap = (struct MinHeap *)malloc(sizeof(struct MinHeap));
    minHeap->size = 0;
    minHeap->capacity = capacity;
    minHeap->array = (struct Node **)malloc(capacity * sizeof(struct Node *));
    return minHeap;
}

//Menukar posisi dua node dalam min heap
void swapNodes(struct Node **a, struct Node **b) {
    struct Node *temp = *a;
    *a = *b;
    *b = temp;
}

//Melakukan operasi heapify pada min heap
void minHeapify(struct MinHeap *minHeap, int idx) {
    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;

    if (left < minHeap->size && minHeap->array[left]->frequency < minHeap->array[smallest]->frequency) {
        smallest = left;
    }

    if (right < minHeap->size && minHeap->array[right]->frequency < minHeap->array[smallest]->frequency) {
        smallest = right;
    }

    if (smallest != idx) {
        swapNodes(&minHeap->array[idx], &minHeap->array[smallest]);
        minHeapify(minHeap, smallest);
    }
}

//Mengembalikan 1 jika ukuran heap adalah 1, dan 0 jika tidak
int isSizeOne(struct MinHeap *minHeap) {
    return (minHeap->size == 1);
}

//Mengeluarkan (extract) node dengan frekuensi terkecil dari min heap.
struct Node *extractMin(struct MinHeap *minHeap) {
    struct Node *temp = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1];
    --minHeap->size;
    minHeapify(minHeap, 0);
    return temp;
}

//Menyisipkan (insert) sebuah node baru ke dalam min heap.
void insertMinHeap(struct MinHeap *minHeap, struct Node *node) {
    ++minHeap->size;
    int i = minHeap->size - 1;
    while (i && node->frequency < minHeap->array[(i - 1) / 2]->frequency) {
        minHeap->array[i] = minHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }
    minHeap->array[i] = node;
}

//Membangun min heap dari array node-node yang ada dalam heap.
void buildMinHeap(struct MinHeap *minHeap) {
    int n = minHeap->size - 1;
    int i;
    for (i = (n - 1) / 2; i >= 0; --i) {
        minHeapify(minHeap, i);
    }
}

//Membangun pohon Huffman berdasarkan frekuensi karakter yang diberikan
struct Node *buildHuffmanTree(int *freq) {
    struct Node *left, *right, *top;
    struct MinHeap *minHeap = createMinHeap(MAX_SIZE);

    for (int i = 0; i < MAX_SIZE; ++i) {
        if (freq[i] > 0) {
                        insertMinHeap(minHeap, createNode((char)i, freq[i]));
        }
    }

    buildMinHeap(minHeap);

    while (!isSizeOne(minHeap)) {
        left = extractMin(minHeap);
        right = extractMin(minHeap);

        top = createNode('$', left->frequency + right->frequency);
        top->left = left;
        top->right = right;

        insertMinHeap(minHeap, top);
    }

    struct Node *root = extractMin(minHeap);
    return root;
}

//Melakukan encoding Huffman pada pohon Huffman yang telah dibangun
void encodeHuffman(struct Node *root, char *code, int top, char **huffmanCodes) {
    if (root->left) {
        code[top] = '0';
        encodeHuffman(root->left, code, top + 1, huffmanCodes);
    }

    if (root->right) {
        code[top] = '1';
        encodeHuffman(root->right, code, top + 1, huffmanCodes);
    }

    if (!root->left && !root->right) {
        code[top] = '\0';
        huffmanCodes[root->character] = strdup(code);
    }
}

//Melakukan kompresi pada file 
void compressFile(FILE *file, struct Node *root, int *freq, int fd[]) {
    char *huffmanCodes[MAX_SIZE] = {0};
    char code[MAX_SIZE];

    encodeHuffman(root, code, 0, huffmanCodes);

    rewind(file);

    char bitBuffer = 0;
    int bitCount = 0;

    int c;
    while ((c = fgetc(file)) != EOF) {
        if (c >= 'A' && c <= 'Z') {
            char *huffmanCode = huffmanCodes[c];
            int i = 0;
            while (huffmanCode[i] != '\0') {
                bitBuffer <<= 1;
                if (huffmanCode[i] == '1') {
                    bitBuffer |= 1;
                }
                ++bitCount;

                if (bitCount == 8) {
                    write(fd[1], &bitBuffer, sizeof(char));
                    bitBuffer = 0;
                    bitCount = 0;
                }

                ++i;
            }
        }
    }

    if (bitCount > 0) {
        bitBuffer <<= (8 - bitCount);
        write(fd[1], &bitBuffer, sizeof(char));
    }

    
// Mengirim sinyal terminasi untuk menandakan akhir dari proses kompresi
    char terminationSignal = '\0';
    write(fd[1], &terminationSignal, sizeof(char));
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: %s [filename]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int fd[2];
    pid_t pid;
    char buffer[MAX_SIZE];

    if (pipe(fd) == -1) {
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (pid == 0) { // child process
        close(fd[1]); // tutup write end
        FILE *out = fopen("compressed.txt", "w");

        // baca frekuensi dari parent process
        while (read(fd[0], buffer, MAX_SIZE) > 0) {
            // lakukan kompresi Huffman
            // kemudian tulis ke file compressed.txt
            fprintf(out, "%s", buffer);
        }

        fclose(out);
        close(fd[0]); // tutup read end
        exit(EXIT_SUCCESS);
    } else { // parent process
        close(fd[0]); // tutup read end
        FILE *file = fopen(argv[1], "r");

        // hitung frekuensi kemunculan huruf
        int freq[MAX_SIZE] = {0};
        int c;
        while ((c = fgetc(file)) != EOF) {
            if (c >= 'a' && c <= 'z') {
                c -= 32; // ubah huruf kecil ke huruf besar
            }
            if (c >= 'A' && c <= 'Z') {
                freq[c]++;
            }
        }

        // kirim frekuensi ke child process
        char freq_str[MAX_SIZE];
        for (int i = 0; i < MAX_SIZE; i++) {
            if (freq[i] > 0) {
                sprintf(freq_str, "%d:%d\n", i, freq[i]);
                write(fd[1], freq_str, strlen(freq_str));
            }
        }

        fclose(file);
        close(fd[1]); // tutup write end

        // tunggu child process selesai
        wait(NULL);

        // Buka file compressed.txt yang telah dihasilkan
        FILE *compressedFile = fopen("compressed.txt", "r");

        // Baca isi file compressed.txt dan kirim ke program dekompresi menggunakan pipe
        while (fgets(buffer, MAX_SIZE, compressedFile) != NULL) {
            write(fd[1], buffer, strlen(buffer));
        }

        fclose(compressedFile);
        close(fd[1]); // tutup write end
        wait(NULL); // tunggu child process dekompresi selesai

        exit(EXIT_SUCCESS);
    }

    return 0;
}
